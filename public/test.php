<?php

$param=$_GET['name'];



require_once '../vendor/autoload.php';
use PSolr\Client\SolrClient;
use PSolr\Request as Request;

// Connect to a Solr server.
$solr = SolrClient::factory(array(
'base_url' => 'http://10.0.2.2:8983', // defaults to "http://localhost:8983"
'base_path' => '/solr/demo',           // defaults to "/solr"
));

$select = Request\Select::factory()
->setQuery($param)
->setStart(0)
->setRows(200)
;

$response = $select->sendRequest($solr);
$response->numFound();

$docs = ($response["response"]["docs"]);

$file_handle = fopen("standard.txt", "rb");


echo "Result By Query";
foreach ($docs as $doc){
    echo '<pre>' .$doc["DOCID"] .'</pre>';
    echo "\n";
}

echo "Result By Standard Data";
while (!feof($file_handle) ) {

    $line_of_text = fgets($file_handle);
    $parts = explode('=', $line_of_text);
    print '<pre>' .$parts[0] . '</pre>';
}

fclose($file_handle);




?>

