<!DOCTYPE html>
<html>
<head>
    <title>Universität Duisburg Essen</title>
    <script type="text/javascript" src="/js/libs/jquery-1.9.0/jquery.min.js"></script>
    <script type="text/javascript" src="/js/libs/twitter-bootstrap-2.2.2/bootstrap.min.js"></script>
    <script type="text/javascript" src="/js/searchengine.js"></script>
    <script type="text/javascript" src="/Lettering.js-master/jquery.lettering.js"></script>
    <link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.2.2/css/bootstrap-combined.min.css" rel="stylesheet">
    <link href="/css/styles.css" rel="stylesheet" />
    <link href="animate.css" rel="stylesheet" />

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">


    <link href="/textillate-master/assets/animate.css" rel="stylesheet">
    <link href="/textillate-master/assets/style.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Rokkitt' rel='stylesheet' type='text/css'>

    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet"/>
    <link href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css" rel="stylesheet"/>
    <script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>

    <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">

    <style>
        html, body {
            height: 100%;
        }

        body {
            margin: 0;
            padding: 0;
            width: 100%;
            display: table;
            font-weight: 100;
            font-family: 'Lato';
        }

        .container {
            text-align: center;
            display: table-cell;
            vertical-align: middle;
        }

        .content {
            text-align: center;
            display: inline-block;
        }

        .title {
            font-size: 96px;
        }
    </style>
</head>
<body>
<div class="container">
    <div class="content">
        <div class="title">Internet-Suchmaschinen Praktische Übung</div>
        <a href="/index" class="btn btn-info" role="button">Get Strated</a>
    </div>
</div>
</body>
</html>

<script>
    $( document ).ready(function() {
        $("div").addClass("fadeInDown animated").one('animationend webkitAnimationEnd oAnimationEnd', function() {
        });
    });
</script>
