<!DOCTYPE html>
<html>
<head>
    <title>Internet Suchmaschinen Praktische Übung</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <script type="text/javascript" src="/js/libs/jquery-1.9.0/jquery.min.js"></script>
    <script type="text/javascript" src="/js/libs/twitter-bootstrap-2.2.2/bootstrap.min.js"></script>
    <script type="text/javascript" src="/js/searchengine.js"></script>
    <script type="text/javascript" src="/Lettering.js-master/jquery.lettering.js"></script>
    <link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.2.2/css/bootstrap-combined.min.css" rel="stylesheet">
    <link href="/css/styles.css" rel="stylesheet" />
    <link href="animate.css" rel="stylesheet" />

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">


    <link href="/textillate-master/assets/animate.css" rel="stylesheet">
    <link href="/textillate-master/assets/style.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Rokkitt' rel='stylesheet' type='text/css'>

    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet"/>
    <link href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css" rel="stylesheet"/>
    <script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>

</head>
<body>
<h3 align="center">Internet Suchmaschinen Praktische Übung</h3>

<ul class="nav nav-pills">
    <li role="presentation"><a href="/">Home</a></li>
    <li role="presentation" class="active"><a href="/index">Dashboard</a></li>
</ul>

<br>
<div class="container-fluid">
    <div id="CLEF" class="alert alert-success" role="alert">
        <strong>CLEF Collection</strong> All Files are loaded.
    </div>

    <form method="GET" name="myform" action="test.php?action=contact_agent&agent_id="  onsubmit="SetData()">
        <div class="form-group">
            <label for="options">Query Parameter</label>
            <select class="form-control" id="agent" name="agent" required multiple>
                <option>Letter Bomb for Kiesbauer</option>
                <option>Find information on the explosion of a letter bomb in the studio of the TV channel PRO7 presenter Arabella Kiesbauer</option>
                <option>A letter bomb from right-wing radicals sent to the black TV personality Arabella Kiesbauer exploded in a studio of the TV channel PRO7 on June 9th, 1995. An assistant was injured. All reports on the explosion and police inquiries after the event are relevant. Other reports on letter bomb attacks are of no interest</option>
            </select>
        </div>
        <button type="submit" class="btn btn-primary" onclick="return check()">Execute Query</button>

        <button type="submit" class="btn btn-primary" onclick="return check2()">Stop Words</button>
        <button type="submit" class="btn btn-primary" onclick="return check3()">Stemming</button>


    </form>
    <a href="qrels_EN" download="qrels_EN.txt"><button class="btn btn-primary" onclick="generate()">Generate qrels_EN</button></a>
    <a href="sgmldocument.xml" download="sgmldocument.xml"><button class="btn btn-primary" onclick="generate2()">Generate XMLData</button></a>

    <br>
    <br>
    <p id="msg"></p>

</div>
</body>
</html>


<script>
    $( document ).ready(function() {
        $("#CLEF").addClass("fadeInDown animated").one('animationend webkitAnimationEnd oAnimationEnd', function() {
        });
        toastr["info"]("Apache Solr Initialized at localhost with Port : 8983", "Initialization",{"progressBar": false,"timeOut": "6000"});
    });
    /*function SetData(){
        var select = document.getElementById('agent');
        var agent_id = select.options[select.selectedIndex].value;
        console.log(select);
        document.myform.action = "test.php?action=contact_agent&agent_id="+agent_id;
        myform.submit();
    }*/
    function check(){
        //$('p').removeClass();
        //$('p').addClass('animated fadeInRight');

        $("p").addClass("fadeInDown animated").one('animationend webkitAnimationEnd oAnimationEnd', function() {
            $("p").removeClass();
        });

        var value=document.getElementById('agent').value;
        var dataString= value;
        toastr["success"]("Parameter For Query : "+value, "Initialization");
        console.log(dataString);
        $.ajax({
            type:"get",
            url:"test.php?name="+value,
            data: dataString,
            cache: false,
            success : function (html) {
                $("#msg").html(html);
            }
        });
        return false;
    }

    function check2(){
        //$('p').removeClass();
        //$('p').addClass('animated fadeInRight');

        $("p").addClass("fadeInDown animated").one('animationend webkitAnimationEnd oAnimationEnd', function() {
            $("p").removeClass();
        });

        var value=document.getElementById('agent').value;
        var ret = value.replace('for','');
        var dataString= ret;
        toastr["success"]("Parameter For Query : "+value, "Initialization");
        //var ret = "data-123".replace('data-','');
        //console.log(ret);
        console.log(dataString);
        $.ajax({
            type:"get",
            url:"test.php?name="+value,
            data: dataString,
            cache: false,
            success : function (html) {
                $("#msg").html(html);
            }
        });
        return false;
    }

    function check3(){
        //$('p').removeClass();
        //$('p').addClass('animated fadeInRight');

        $("p").addClass("fadeInDown animated").one('animationend webkitAnimationEnd oAnimationEnd', function() {
            $("p").removeClass();
        });

        var value=document.getElementById('agent').value;
        var dataString= value;
        toastr["success"]("Parameter For Query : "+value, "Initialization");
        console.log(dataString);
        $.ajax({
            type:"get",
            url:"test.php?name="+value,
            data: dataString,
            cache: false,
            success : function (html) {
                $("#msg").html(html);
            }
        });
        return false;
    }


    function generate(){
        toastr["success"]("Generating File qrels_EN for all topics", "File Generated");

    }

    function generate2(){
        toastr["success"]("Generating File sgml into xml", "File Generated");

    }

</script>